#!/bin/bash
if [ ! -d plantuml ]; then
    mkdir plantuml
fi
cd plantuml
if [ ! -f plantuml.zip ]; then
    curl -o plantuml.zip https://netcologne.dl.sourceforge.net/project/plantuml/1.2020.0/plantuml-jar-asl-1.2020.0.zip
fi
unzip -o plantuml.zip
cd ..
java -jar plantuml/plantuml.jar "**.plantuml"